import Vue from 'vue';
import Vuetify, {
  VApp,
  VToolbar,
  VLayout,
  VFlex,
  VImg,
  VIcon,
  VContent,
  VContainer,
  VSpacer,
  VBtn,
  VToolbarTitle,
  VFooter,
  VForm,
  VTextField,
  VList,
  VListTile,
  VListTileAvatar,
  VListTileContent,
  VListTileAction,
  VDialog,
} from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';
import pl from 'vuetify/es5/locale/pl';
import en from 'vuetify/es5/locale/en';


Vue.use(Vuetify, {
  iconfont: 'md',
  lang: {
    locales: { pl, en },
    current: 'pl',
  },
  components: {
    VApp,
    VToolbar,
    VLayout,
    VFlex,
    VImg,
    VIcon,
    VContent,
    VContainer,
    VSpacer,
    VBtn,
    VToolbarTitle,
    VFooter,
    VForm,
    VTextField,
    VList,
    VListTile,
    VListTileAvatar,
    VListTileContent,
    VListTileAction,
    VDialog,
  },
})
