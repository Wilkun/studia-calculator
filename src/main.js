import Vue from 'vue';
import './plugins/vuejs-logger';
import './plugins/vuetify';

import App from './App.vue';
import './registerServiceWorker';

Vue.config.productionTip = false;

Vue.config.keyCodes = {
  in0: [48, 96],
  in1: [49, 97],
  in2: [50, 98],
  in3: [51, 99],
  in4: [52, 100],
  in5: [53, 101],
  in6: [54, 102],
  in7: [55, 103],
  in8: [56, 104],
  in9: [57, 105],
  multiply: 106,
  add: 107,
  subtract: 109,
  decimal_point: [110, 188],
  divide: 111,
  open_bracket: 219,
  close_bracket: 221,
};

new Vue({
  render: h => h(App),
}).$mount('#app');
